# grunt task to autoresize images
- 930x
- 690x
- 510x
- original size on click

# shortcodes implementations
- https://github.com/gohugoio/hugo/blob/master/tpl/tplimpl/template_embedded.go

# fixing old entries
- 2005
- 2006
- 2007
- 2008
- 2010
- 2009
- 2011
- 2012
- 2013
- 2014
- 2015
- 2016
- 2017

# todo import social media
- instagram
- facebook
- twitter
- lastfm

# implement AMP
- https://github.com/wildhaber/gohugo-amp
- https://gohugo-amp.gohugohq.com/shortcodes/
- https://gohugo-amp.gohugohq.com/schema/

# new content
https://www.google.com/doodles/payom-sinawats-109th-birthday

# tags
- create overview page
- check if navigation is working on tag item pages more than 5 posts
- tag cloud

# archive
- do a yearly overview (manual)
- do a monthly overview per year (probably manual?)

# comments
- https://staticman.net
- https://github.com/eduardoboucas/staticman
- https://networkhobo.com/2017/12/30/hugo---staticman-nested-replies-and-e-mail-notifications/
- https://mademistakes.com/articles/jekyll-static-comments/

# check opengraph
- https://developers.facebook.com/tools/debug/sharing
- https://developers.facebook.com/docs/sharing/opengraph/object-properties
- http://ogp.me

# resizing image formats
- facebook share images (og:image): 600x315 1200x630 or larger, but 1.91:1 aspect ratio 

# twitter
- embedder: https://publish.twitter.com/#


https://github.com/gohugoio/hugo/blob/ff28120e53c7d3d5fce9941082f72eb89adb43b7/tpl/template_embedded.go