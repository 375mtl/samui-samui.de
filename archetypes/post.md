---
title: "{{ replace .TranslationBaseName "-" " " | title }}"
description: ""
author: Patrick Kollitsch
type: post
date: {{ .Date }}
lastmod: {{ .Date }}
url:
aliases:
- url
tags:
- tag1
- tag2
linktitle: "use for linking to this post"
resources :
- src: "file"
  name: "name"
  title: "title"
---
