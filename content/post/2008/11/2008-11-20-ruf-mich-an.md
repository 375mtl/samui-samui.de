---
title: Ruf! Mich! An!
author: Patrick Kollitsch
type: post
date: 2008-11-20T00:22:59+00:00
url: /2008/11/ruf-mich-an/




---
Man kann mit [Skype][1] verdammt preiswert nach Thailand telefonieren. Ich habe mir eben mal angekuckt, was man 'von &uuml;berall auf der Welt' nach Thailand bezahlen darf (nach Deutschland zu telefonieren ist leicht teurer):

<table style="width:98%;" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <th>
      Ziel
    </th>
    
    <th>
      Nettopreis
    </th>
    
    <th>
      inkl. Umsatzsteuer*
    </th>
  </tr>
  
  <tr>
    <td>
      <a href="http://www.skype.com/prices/callrates/?currency=EUR#listing-T">Thailand</a>
    </td>
    
    <td>
      &euro; 0.092
    </td>
    
    <td>
      &euro; 0.106
    </td>
  </tr>
  
  <tr>
    <td>
      Thailand-Bangkok (+662...)
    </td>
    
    <td>
      &euro; 0.050
    </td>
    
    <td>
      &euro; 0.058
    </td>
  </tr>
  
  <tr>
    <td>
      Thailand --- Mobile
    </td>
    
    <td>
      &euro; 0.093
    </td>
    
    <td>
      &euro; 0.107
    </td>
  </tr>
  
  <tr>
    <td>
      <a href="http://www.skype.com/prices/smsrates/?currency=EUR#listing-T"><span class="caps">SMS</span> nach Thailand</a>
    </td>
    
    <td>
      &euro; 0.051
    </td>
    
    <td>
      &euro; 0.059
    </td>
  </tr>
</table>

<small>* muahahahaha, aus Europa darf man Umsatzsteuer drauf zahlen (oder wars die MwSt.?)</small>

Festnetztelephone sind &uuml;brigens eher selten in Thailand, der Drang geht zu einem oder mehreren Mobiltelefonen. Oder eben Skype.

PS: Wer mich anrufen (oder besser anchatten) will, kann das gerne versuchen. Das Problem ist, dass man meistens schon dabei scheitert, mich zu seinen Kontakten hinzuzuf&uuml;gen, weil ich recht gerne aussage kr&auml;ftige Anfragen erhalte. Mein Skype-Username ist <a href="skype:kollitsch?add" onclick="return skypeCheck();"><img src="http://mystatus.skype.com/smallicon/kollitsch" /> kollitsch</a>. Oh, und nein, ich kann kein preiswertes Hotel empfehlen, ich weiss nicht, in welcher Branche man sich auf der Insel selbständig machen kann und von Visaruns habe ich schon gar keine Ahnung. Wirklich.

 [1]: http://skype.com