---
title: Berg unter Feuer
author: Patrick Kollitsch
type: post
date: 2016-05-11T09:36:54+00:00
url: /2016/05/berg-unter-feuer/


tags:
  - waldbrand

---
Seit gestern Abend brennt es auf dem Berg zwischen Maenam und Bophut, und zwar ziemlich heftig. Da man in Thailand alles nur in Relationen verstehen kann, verk&uuml;ndeten die News heute, dass es der schlimmste Grossbrand der vergangenen 50 Jahre auf Samui ist. Erschwert werden die L&ouml;schversuche durch die Unzug&auml;nglichkeit des Gebietes. 

Wie es zum Brand kam, kann man noch nicht so genau sagen. Die Spekulationen gehen aber dahin, dass da Brandrodung betrieben wurde. Andererseits kann es auch durchaus sein, dass eine ung&uuml;nstig als Abfall abgelegte Flasche in Verbindung mit ein wenig Sonneneinstrahlung und optischen Grundgesetzen das durch eine seit 8 Wochen anhaltende D&uuml;rre leicht entflammbare Laub entflammte(n?). 

Erw&auml;hnte D&uuml;rre-Periode ist &uuml;brigens die l&auml;ngste und w&auml;rmste der vergangenen 65 Jahre. (Relationen, klar...)

Hier noch ein Video (auf Thai) mit n&auml;chtlichem Brand.

<div class="video-youtube embed-responsive-item" id="video-youtube-48054b9a0f4eeeb163af72fdb30eb1c4" data-video="//www.youtube.com/embed/5v9dniq2N60?&loadvideo=&autohide=2&autoplay=1&rel=0&controls=2&color=red&modestbranding=1&iv_load_policy=3&theme=light&enablejsapi=1&origin=https://localhost">
  <img src="/wp-content/imagecache/5v9dniq2N60-hqdefault.jpg" alt="video preview" /><span class="video-youtube-play-icon" aria-label="Play this video"><i class="icon-play" aria-hidden="true"></i></span>
</div>