---
title: Bloss nicht in Thailand
author: Patrick Kollitsch
type: post
date: 2016-05-08T06:51:24+00:00
url: /2016/05/bloss-nicht-in-thailand/
featured_image: /wp-content/uploads/2016/05/bloss-nicht-in-thailand/content-unavailable-in-thailand.png


tags:
  - facebook
  - meinungsfreiheit

---
Die Medien bezeichnen als "bisher nicht vorgekommen", was meiner Meinung nach schon lange Tatsache ist: Facebook blockt in Thailand Seiten, die Regierungs- oder Monarchie-kritisch sind. Zuletzt geschehen mit [einer Seite][1], die "hin und wieder" einmal die thail&auml;ndische Monarchie verspottet hat. In Zeiten wo es Verhaftungen aufgrund von Facebookeintr&auml;gen gibt nicht sonderlich erstaunlich, aber erw&auml;hnenswert. Zu schade nur, dass Facebook klein bei gibt. "Freies Internet f&uuml;r alle" und so weiter.

<i class="icon-globe" aria-hidden="true"></i> [More Informationen][2] (in Englisch)

 [1]: http://www.facebook.com/gukultresurrection
 [2]: http://www.khaosodenglish.com/detail.php?newsid=1462426398&section=11