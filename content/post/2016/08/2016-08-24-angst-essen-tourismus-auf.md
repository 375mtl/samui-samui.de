---
title: Angst essen Tourismus auf
author: Patrick Kollitsch
type: post
date: 2016-08-24T02:33:38+00:00
url: /2016/08/angst-essen-tourismus-auf/
instant_articles_submission_id:
  - "204247676709332"


tags:
  - bombenanschlag
  - terrorismus

---
Wenn man [die News im Spiegel heute][1] so liest, mag man meinen, dass es eine schlechte Idee ist, Thailand derzeit zu besuchen: 

> Im Süden Thailands sind in der Nähe eines Hotels zwei Autobomben explodiert. Medienberichten zufolge wurde dabei eine Frau getötet, mindestens 29 Menschen sind verletzt.

Der reisserische Text impliziert geradezu, dass jetzt auch die Hotels be-bombt werden. Liest man sich aber ein bisschen tiefer in die Materie ein, kommt schnell heraus, dass "im Süden Thailands" Bombenanschläge an der Tagesordnung sind. Dort kämpfen Separatisten seit über 10 Jahren darum, ein eigenes Kalifat zu errichten. Selbst das Auswärtige Amt weist in seinen [Reise- und Sicherheitshinweisen zu Thailand][2] darauf (ebenfalls seit Jahren) hin.

 [1]: http://www.spiegel.de/panorama/justiz/thailand-offenbar-ein-toter-und-mehrere-verletzte-bei-bombenanschlag-a-1109163.html
 [2]: https://www.auswaertiges-amt.de/DE/Laenderinformationen/00-SiHi/ThailandSicherheit.html