---
title: Indiana Jones und der Tempel der Tiger
author: Patrick Kollitsch
type: post
date: 2016-06-02T15:19:56+00:00
url: /2016/06/indiana-jones-und-der-tempel-der-tiger/
instant_articles_submission_id:
  - "1098924053525245"


tags:
  - tempel
  - tiger
  - tigertempel

---
<figure id="attachment_4648" style="max-width: 360px" class="wp-caption alignleft">[<img src="/site/2016/06/indiana-jones-und-der-tempel-der-tiger/Cj7f9WqVAAAiqPj-360x270.jpg" alt="Pickled Tigers from Kanchanaburi, via Richart Miyer" width="360" height="270" class="size-thumbnail wp-image-4648" srcset="/wp-content/uploads/2016/06/indiana-jones-und-der-tempel-der-tiger/Cj7f9WqVAAAiqPj-360x270.jpg 360w, /wp-content/uploads/2016/06/indiana-jones-und-der-tempel-der-tiger/Cj7f9WqVAAAiqPj.jpg 600w" sizes="(max-width: 360px) 100vw, 360px" />][1]<figcaption class="wp-caption-text">Pickled Tigers from Kanchanaburi, via [Richart Miyer][2]</figcaption></figure> 

Eine besonders beliebte Attraktion in Thailand ist der ber&uuml;hmte Tiger-Tempel Luangta Maha Bua in Kanchanaburi. Ich kenne keinen Thailand-Reisenden der nicht gerne einen Selfie mit einem Tiger in seine Social Media Str&ouml;me stellt. Dummerweise nur gibt es seit Jahren "Tiersch&uuml;tzer", die gegen diesen Tempel und die Praxis demonstrieren. 

Als Urlauber, nach einem besondern Photo suchend, sollte man sich meiner Meinung nach immer der Tatsache bewusst sein, dass so ein Photo nicht _echt_ sein kann. In jedweder Hinsicht.

Jedenfalls haben nun auch die "Zoobeh&ouml;rden" Thailands nach vielen Jahren reagiert und dem Tempel untersagt, Tiger zu halten. Seit ein paar Tagen werden die Tiere (mehr oder weniger freiwillig und ohne Zwischenf&auml;lle) abtransportiert. 

Interessant ist, was am Rande auftaucht. Beispielsweise [40 eingefrorene Tiger-Babies][3], oder ein M&ouml;nch (nicht mit dem europ&auml;ischen Begriff M&ouml;nch zu verwechseln), [der mit Tigerfellen auf der Flucht ertappt wird][4]. 

Kurzum: Bist auf Urlaubsreise und bieten sie dir ein "einzigartiges" Erlebnis mit "echten" wilden Tieren an, dann ist es meistens sehr unangenehm f&uuml;r die Tiere und sp&auml;ter f&uuml;r dich --- wenn du ein Gewissen hast.

 [1]: /site/2016/06/indiana-jones-und-der-tempel-der-tiger/Cj7f9WqVAAAiqPj.jpg
 [2]: https://twitter.com/Richart_NOW26/status/738273658137804801
 [3]: http://www.spiegel.de/panorama/thailand-40-tote-tigerbabys-in-tempel-gefunden-a-1095235.html
 [4]: https://asiancorrespondent.com/2016/06/thailand-tiger-temple-charged-animal-products/