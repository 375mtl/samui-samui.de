---
title: David Bowie in Bangkok
author: Patrick Kollitsch
type: post
date: 2016-01-12T12:28:32+00:00
url: /2016/01/david-bowie-in-bangkok/


tags:
  - bangkok
  - david bowie
  - video

---
<div class="video-youtube embed-responsive-item" id="video-youtube-29bfd319371e0a297c2475970ceabd91" data-video="//www.youtube.com/embed/NmIDE_feJUs?&loadvideo=&autohide=2&autoplay=1&rel=0&controls=2&color=red&modestbranding=1&iv_load_policy=3&theme=light&enablejsapi=1&origin=https://localhost">
  <img src="/wp-content/imagecache/NmIDE_feJUs-hqdefault.jpg" alt="video preview" /><span class="video-youtube-play-icon" aria-label="Play this video"><i class="icon-play" aria-hidden="true"></i></span>
</div>