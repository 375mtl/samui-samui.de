---
title: 'Kinder- und Lehrertag'
author: Patrick Kollitsch
type: post
date: 2017-01-16T13:32:23+00:00
url: /2017/01/kinder-und-lehrertag/


tags:
  - feiertage
  - video

---
Am zweiten Samstag im Januar findet hierzulande der Kindertag statt. Während anderswo dieser Tag genutzt wird, um auf die Bedürfnisse von Kindern und Probleme speziell mit Kinderrechten hinzuweisen und einzugehen, ist er hier in Thailand ein Tag, um Maschinengewehre in jungem Alter in die Hände zu nehmen und auf Panzern herumzuklettern. Indoktrination beginnt nun mal früh.

Dieses Jahr war der Tag ein wenig aufregender, weil in Hat Yai auf einer Flugschau ein Flugzeug abgestürzt ist.

<div class="video-youtube embed-responsive-item" id="video-youtube-3ef1f1962d3426c3c5b7be84cd19fcab" data-video="//www.youtube.com/embed/081NOLjBD3k?&loadvideo=&autohide=2&autoplay=1&rel=0&controls=2&color=red&modestbranding=1&iv_load_policy=3&theme=light&enablejsapi=1&origin=https://localhost">
  <img src="/wp-content/imagecache/081NOLjBD3k-hqdefault.jpg" alt="video preview" /><span class="video-youtube-play-icon" aria-label="Play this video"><i class="icon-play" aria-hidden="true"></i></span>
</div>

Am 16. Januar jedes Jahr dann wiederum (passenderweise immer ein paar Tage nach dem Kindertag) ist es dann an der Zeit, die Lehrer zu ehren. Genaugenommen gibt es in jedem Lebensbereich Lehrer, sei es in der Schule (des Lebens), der Universitaet, im Sport und vielen anderen Bereichen. Der Lehrertag ist daher ein Grund für alle, den Lehrern in ihrem Leben Respekt zu erweisen.

&nbsp;