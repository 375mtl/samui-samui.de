---
title: 'Google Doodle des Tages: Kindertag in Thailand'
author: Patrick Kollitsch
type: post
date: 2017-01-14T03:54:45+00:00
url: /2017/01/google-doodle-des-tages-kindertag-in-thailand/
instant_articles_submission_id:
  - "227066637701107"


tags:
  - google doodle

---
<figure id="figure-4727" class="media-4727"> 

<img class="align-left" src="/wp-content/2017/01/google-doodle-des-tages-kindertag-in-thailand/childrens-day-2017-thailand-5735837015736320-hp-360x144.jpg" srcset="/wp-content/2017/01/google-doodle-des-tages-kindertag-in-thailand/childrens-day-2017-thailand-5735837015736320-hp-360x144.jpg 360w, /wp-content/2017/01/google-doodle-des-tages-kindertag-in-thailand/childrens-day-2017-thailand-5735837015736320-hp.jpg 550w" sizes="" alt="Kindertag 2017 in Thailand" /><figcaption>Google Doodle Kindertag 2017 in Thailand --- &copy; google.com</figcaption></figure>