---
title: Bangkok, der Naturpark
author: Patrick Kollitsch
type: post
date: 2017-01-27T13:53:12+00:00
url: /2017/01/naturpark-bangkok/
featured_image: /wp-content/uploads/2017/01/bangkok-der-naturpark/bangkok-in-google-maps-20170127.png
instant_articles_submission_id:
  - "170242700129387"


tags:
  - bangkok
  - google
  - maps

---
Google Maps hat heute morgen Bangkok zum Naturpark ernannt. Wenn man ganz weit aus der Map heraus zoomt, bleibt die Bezeichnung "Thung Yai Naresuan Wildlife Sanctuary" sichtbar. So wie es aussieht hat da jemand aus Versehen (oder auch nicht) die Grenzen des Naturparks erweitert. Soviel Grün wird man wahrscheinlich erst in ein paar hundert Jahren, wenn die Affen die Welt regieren, in dieser Gegend sehen.

Allerdings: Als man bei den Parkbeamten telephonisch anfragte, ob Bangkok jetzt zum Naturpark gehört, hat dieser tatsächlich nicht gewusst, ob das so sei. Wirklich.

[via [Khaosod][1]]

 [1]: http://www.khaosodenglish.com/culture/net/2017/01/27/much-green-google-declares-bangkok-parkland/