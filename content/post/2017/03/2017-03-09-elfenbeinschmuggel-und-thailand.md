---
title: Elfenbeinschmuggel und Thailand
author: Patrick Kollitsch
type: post
date: 2017-03-09T14:30:58+00:00
url: /2017/03/elfenbeinschmuggel-und-thailand/
featured_image: /wp-content/uploads/2017/03/elfenbeinschmuggel-und-thailand/Thailand-Ivory-Seized_Cham.jpg
instant_articles_submission_id:
  - "278236139280106"


tags:
  - elfenbein
  - kriminalität

---
Thaibehörden haben 422 Elefanten-Stosszähne konfisziert und einen Gamber (Gambanesen? Gambier?) festgenommen, der die Lieferung in Empfang nehmen wollte. Deklariert waren "ungeschliffene Edelsteine", aber es davon fand man nicht viele in den Kisten. 330 Kilogramm Elefanten-Stosszähne, wert gut eine halbe Million US-Dollar. 

Thailand war jahrelang Hauptumschlagplatz fuer solche Lieferungen, mit China als Haupt-Kunde. Das Image Thailands als Zentrum des Elfenbeinhandels war jahrelang ein Gesichtsverlust. Erst 2014 wurden Gesetze erlassen, die Elfenbeinhandel in Thailand kriminalisieren. 

Mehr Info (auf Englisch):
  
--- [Khaosod][1]
  
--- [Thai PBS][2]

 [1]: http://www.khaosodenglish.com/news/crimecourtscalamity/crime-crime/2017/03/08/gambian-man-arrested-422-pieces-smuggled-elephant-tusks/
 [2]: http://englishnews.thaipbs.or.th/customs-seized-17-million-baht-worth-african-ivory/