---
title: Party-Insel Nummer 4
author: Patrick Kollitsch
type: post
date: 2015-12-28T09:50:49+00:00
url: /2015/12/party-insel-nummer-4/
featured_image: /wp-content/uploads/2015/12/party-insel-nummer-4/rca-2015-party-islands-koh-samui-conde-nast.jpg


tags:
  - awards
  - party

---
Koh Samui hat im begehrten Readers&#8216; Choice Awards 2015 des Condé Nast Traveler Magazins in der Kategorie der 10 Besten Party-Inseln in der Welt einen 4. Platz belegt.

Sie schreiben:

> At nearly five miles long, gorgeous Chaweng Beach is the largest of Koh Samui’s beaches. It’s lined with coconut trees and boasts silvery sand, turquoise waters, and even a natural reef. Added bonus: Its northern end is party central. Though Chaweng Beach Road has plenty to offer in the way of food and beverage, try dining on the beach itself—after sunset, many restaurants set up tables and serve fresh seafood at competitive prices. For nightlife, check out Green Mango, one of Koh Samui’s oldest and most famous clubs.

Dass in der Liste von 10 Inseln allein 4 thailändische Inseln vertreten sind (Samui davon mit der besten Position, natürlich) spricht für sich selbst.

Seltsam nur, dass Koh Tao nicht (mehr) auftaucht. Oder auch nicht, abhängig davon, welchen News man folgt...