---
title: 'Ko Phi Phi in Thailand: Nichts Neues nach dem Tsunami'
author: Patrick Kollitsch
type: post
date: 2015-06-19T12:12:02+00:00
url: /2015/06/ko-phi-phi-in-thailand-nichts-neues-nach-dem-tsunami/




---
Wow. Im Spiegel. Einer dieser tollen wahren grundlagengestützten Artikel über, ehm, ... ja über was eigentlich?

> Thomas ist ein deutscher Tourist und will am tropischen Strand nur feiern. Shila ist Prostituierte und wartet auf Kundschaft. Was sie verbindet? Man könnte sagen, die gleiche Trostlosigkeit.

[<a href="http://www.spiegel.de/reise/fernweh/ko-phi-phi-in-thailand-tourist-und-prostituierte-a-1039473.html" target="_blank">Lies das ganze Werk</a>]