---
title: Fehlgeburt
author: Patrick Kollitsch
type: post
date: 2015-11-18T14:08:11+00:00
url: /2015/11/fehlgeburt/
featured_image: /wp-content/uploads/2015/11/fehlgeburt/lin-hui-at-the-chiang-mai-zoo.jpg


tags:
  - chiang mai
  - panda
  - tierwelt

---
Von den Pandas Chiang Mai's habe ich [schon häufiger][1] im Blog geschrieben. Hin und wieder werden sie schwanger. Dann müssen sie zurück ins (chinesische) Heimatreich, oder auch nicht.

Jedenfalls gibt es jederzeit in Chiang Mai Panda's, die als chinesische Leihgabe für 10 Jahre in den Zoo gegeben werden und dann von hoffnungsvollen Wissenschaftlern und Pflegern aktiv zum Vermehren angeregt werden.

Lin Hui, aktuelle tierische Leihgabe, wurde im Juni "besamt" und hatte ihren Pflegern bereits Hoffnung auf ein weiteres eigenes Panda-Baby gegeben. Leider wurde nichts draus und so hat sie wieder ein paar Monate Ruhe, bevor man das nächste Erfolgserlebnis anstrebt.

[via [Bangkok Post][2]]

 [1]: /thema/panda/
 [2]: http://www.bangkokpost.com/news/general/768072/panda-lin-hui-has-another-miscarriage