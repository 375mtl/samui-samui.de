---
title: Meteor über Bangkok
author: Patrick Kollitsch
type: post
date: 2015-11-03T02:26:27+00:00
url: /2015/11/meteor-ueber-bangkok/


tags:
  - meteor

---
<div class="video-youtube embed-responsive-item" id="video-youtube-a679cb95116dc2be5a0c8a0364baeb98" data-video="//www.youtube.com/embed/Ls1cfDPDGDI?&loadvideo=&autohide=2&autoplay=1&rel=0&controls=2&color=red&modestbranding=1&iv_load_policy=3&theme=light&enablejsapi=1&origin=https://localhost">
  <img src="/wp-content/imagecache/Ls1cfDPDGDI-hqdefault.jpg" alt="video preview" /><span class="video-youtube-play-icon" aria-label="Play this video"><i class="icon-play" aria-hidden="true"></i></span>
</div>

Dashcams sind eine sehr n&uuml;tzliche Erfindung. So kann man n&auml;mlich in diesem Video das Vergl&uuml;hen eines Meteoriten aus allen m&ouml;glichen Richtungen betrachten. Dieser Feuerball war gestern Abend in Bangkok zu sehen. Das gr&uuml;nliche Licht, das beim Verbrennen in der Atmosph&auml;re ausgestrahlt wurde, deutet auf [Eisen und Nickel im Objekt][1] hin. 

Update: Das zuerst verlinkte Video wurde von YouTube entfernt. Tja... so ist das halt wenn man die Filme anderer zusammenschneidert. Habe jetzt ein anderes Video mit nur einer Richtung eingebaut...

 [1]: http://www.nationmultimedia.com/breakingnews/Damage-from-fireball-unlikely-astronomer-30272150.html