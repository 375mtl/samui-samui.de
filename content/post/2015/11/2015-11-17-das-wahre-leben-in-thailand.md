---
title: Das wahre Leben in Thailand
author: Patrick Kollitsch
type: post
date: 2015-11-17T08:57:27+00:00
url: /2015/11/das-wahre-leben-in-thailand/



---
<div class="fb-video" data-allowfullscreen="1" data-href="/TAT.HongKong/videos/vb.150069708363697/918287464875247/?type=3">
  <div class="fb-xfbml-parse-ignore">
    <blockquote cite="https://www.facebook.com/TAT.HongKong/videos/918287464875247/">
      <p>
        <a href="https://www.facebook.com/TAT.HongKong/videos/918287464875247/">Amazing Thailand Luxury: where life rules everything</a>Amazing Thailand Luxury: where life rules everything
      </p>
      
      <p>
        Posted by <a href="https://www.facebook.com/TAT.HongKong/">Tourism Authority of Thailand (HK) / 泰國政府旅遊局 (HK)</a> on Sunday, November 15, 2015
      </p>
    </blockquote>
  </div>
</div>

Diese kurze Video-Dokumentation zeigt das tägliche Leben in Thailand. Ein Genuss. So, muss mich mal eben auf meinen Elephanten schwingen und meinen Zigarrenbestand im 7eleven aufstocken...