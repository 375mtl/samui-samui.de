---
title: Pokki, Katze.
author: Patrick Kollitsch
type: post
date: 2015-07-27T12:46:04+00:00
url: /2015/07/pokki-katze/


tags:
  - artificial intelligence
  - flickr
  - photo
  - pokki

---
<blockquote class="twitter-tweet" lang="en">
  <p lang="en" dir="ltr">
    hey <a href="https://twitter.com/Flickr">@flickr</a>, you need to work on that ;) my babies are outraged... <a href="http://t.co/2SrLee8Ugs">pic.twitter.com/2SrLee8Ugs</a>
  </p>
  
  <p>
    &mdash; Patrick Kollitsch (@davidsneighbour) <a href="https://twitter.com/davidsneighbour/status/625646289531736064">July 27, 2015</a>
  </p>
</blockquote>

Pokki mag Flickr gerade nicht so sehr.