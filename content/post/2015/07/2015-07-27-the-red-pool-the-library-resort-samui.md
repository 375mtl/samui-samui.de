---
title: The Red Pool, The Library Resort Samui
author: Patrick Kollitsch
type: post
date: 2015-07-27T13:28:56+00:00
url: /2015/07/the-red-pool-the-library-resort-samui/


tags:
  - architektur
  - muss man gesehen haben
  - photo
  - the library

---
<a data-flickr-embed="true" href="https://www.flickr.com/photos/schreibblogade/4367437787/in/photolist-7DWhaV-7E17JE-7DWgSe-7DWhrn-7DWgvr-7DWg6v" title="The Library resort in Koh Samui"><img src="https://farm3.staticflickr.com/2721/4367437787_b8b295d71c_b.jpg" width="1024" height="768" alt="The Library resort in Koh Samui" /></a>

Fox-News ist ja eigentlich nicht f&uuml;r clevere Inhalte bekannt. Jetzt hat man dort aber eine [Liste der 8 seltsamsten Hotel-Pools weltweilt][1] ver&ouml;ffentlicht. Und siehe da, Samui ist auf der Liste vertreten. 

Der Pool im Library Resort in Chaweng ist rot. Weshalb man ihn "[The Red Pool][2]" getauft hat. Die interessante F&auml;rbung wird durch rote Kacheln erzeugt. Bei Tag und Nacht ein interessanter Anblick.

 [1]: http://www.foxnews.com/travel/2015/07/24/8-worlds-weirdest-hotel-pools/
 [2]: http://www.thelibrary.co.th/the-red-pool.html