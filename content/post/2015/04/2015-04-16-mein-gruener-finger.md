---
title: Mein grüner Finger
author: Patrick Kollitsch
type: post
date: 2015-04-16T02:43:43+00:00
url: /2015/04/mein-gruener-finger/



tags:
  - blumen
  - orchidee

---
<img src="https://assets.samui-samui.de/2015/04/2015-04-15-08.28.19-1000x750.jpg" alt="2015-04-15 08.28.19" width="1000" height="750" class="img-responsive" />

Ich gebe ja höchst ungern an, aber ich habe es tatsächlich geschafft, nach über drei Jahren vertrockneter Stengel meine Orchidee vorm Haus zum Blühen zu bringen. Ein weiterer Grund mal wieder raus zu gehen...