---
title: Bombenanschlag in Bangkok
author: Patrick Kollitsch
type: post
date: 2015-08-17T13:09:36+00:00
url: /2015/08/bombenanschlag-in-bangkok/


tags:
  - bangkok
  - bombenanschlag

---
Und wieder einmal ist in Bangkok eine Bombe explodiert, diesmal am Erawan-Schrein, einer ziemlich belebten Ecke:

> Im Zentrum der thailändischen Hauptstadt Bangkok hat es eine schwere Explosion gegeben. Eine Bombe sei in der Nähe des Erawan-Schreins an einer Kreuzung im Ratchaprasong-Bezirk explodiert, teilte die Polizei mit. Es würden Opfer vermutet.

Interessanterweise gibt es noch nicht viel zu lesen in einheimischen Medien. Anderswo ist von mindestens 5 Toten und mehr als 20 Verletzten die Rede.

[via <a href="http://www.spiegel.de/panorama/justiz/thailand-bombenanschlag-in-bangkok-a-1048492.html" target="_blank">Spiegel Online</a>]