---
title: Karrieren die Ausländer nicht.
author: Patrick Kollitsch
type: post
date: 2015-08-12T13:41:08+00:00
url: /2015/08/karrieren-die-auslaender-nicht/


tags:
  - lost in translation
  - thaistyle

---
<a data-flickr-embed="true" href="https://www.flickr.com/photos/schreibblogade/20326739948/in/dateposted-public/" title="The Proletariat!"><img src="https://farm1.staticflickr.com/526/20326739948_4629166c55_c.jpg" width="800" height="593" alt="The Proletariat!" /></a>

Neulich hat das Thail&auml;ndische Arbeitsministerium eine Liste der Jobs, die man als Ausl&auml;nder in Thailand nicht aus&uuml;ben darf, ver&ouml;ffentlicht. Zu dumm nur, dass der Azubi bei seiner Bewerbung bei den Angaben zu den Sprachen die er spricht gelogen hat und anscheinend Google Translate genutzt hat. 

Und so gibt es interessante Beschr&auml;nkungen wie bspw:

  * Das Proletariat
  * Den Verkauf jeder Seite
  * Arbeit an goldenen oder silbernen Ottern
  * T&auml;tigkeiten als Thai-Puppe
  * Jobs als Buddha
  * Dinge die man von Hand rollt
  * und viele mehr

Ausser Onlinemarketing bleibt einem da wirklich nicht viel mehr ;)

<a href="https://www.flickr.com/photos/schreibblogade/19893884433/in/dateposted-public/" target="_blank">Seite 2 des Spasses hier</a>.