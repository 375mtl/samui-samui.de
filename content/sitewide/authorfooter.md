---
title: "Patrick Kollitsch"
headless: true
---

{{% figure width="240" height="180" class="authorimage float-left" src="/assets/images/patrick.kollitsch.jpg" alt="Patrick Kollitsch" / %}}

<span class="fn n"><a class="url" href="http://kollitsch.de/"><span class="given-name">Patrick</span> <span class="family-name">Kollitsch</span></a></span> hat die ersten <span class="is-datediff" data-from="1975-07-05"></span> Tage seines Lebens hinter sich und die letzten <span class="is-datediff" data-from="2005-01-08"></span> Tage davon in <span class="adr country-name">Thailand</span> auf Koh Samui, einer kleinen aber ausreichend großen Insel im Golf von Thailand, ge- und verbracht.

Gemeinsam mit <a class="url" href="https://samui-samui.de/tag/shihtzu/">seinem Shih-Tzu</a> Pokki (m, <span class="is-datediff-month" data-from="2005-11-15"></span> Monate alt) bewohnt er ein kleines Haus unter Palmen in Bang Por im Nordwesten der Insel.

Wenn er nicht unter selbigen (den Palmen) an seinem Computer sitzt und die Systeme hinter Webseiten, eierlegenden Wollmilchs&auml;uen und Webspielereien programmiert, streift er über Samui und hält <a class="url" href="http://flickr.com/photos/schreibblogade/tags/wat">die Tempel der Insel</a> und <a href="http://flickr.com/photos/schreibblogade/" class="url">andere Objekte</a> photographisch fest oder versucht, Thai zu lernen.

Er schreibt auf <a class="url" href="https://samui-samui.de/">diesem Weblog</a> über das ganz alltägliche Leben als Farang in Thailand und andere Dinge, die ihn interessieren.
